import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MicroservicesDialogComponent } from './microservices-dialog/microservices-dialog.component';
import { ResourcesDialogComponent } from './resources-dialog/resources-dialog.component';
import { StepsDialogComponent } from './steps-dialog/steps-dialog.component';

// Angular Material
import { MaterialListModule } from '../../../material.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [
    MicroservicesDialogComponent,
    ResourcesDialogComponent,
    StepsDialogComponent
  ],
  entryComponents: [ResourcesDialogComponent],
  imports: [
    CommonModule,
    MaterialListModule,
    MatGridListModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class MvpsModule { }
