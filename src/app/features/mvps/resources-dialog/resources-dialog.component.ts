import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { blankSpaceValidator } from '../../../validators/custom.validators';

@Component({
  selector: 'app-resources-dialog',
  templateUrl: './resources-dialog.component.html',
  styleUrls: ['./resources-dialog.component.css']
})
export class ResourcesDialogComponent implements OnInit {

  form: FormGroup = new FormGroup({
    production: new FormControl(''),
    certification: new FormControl(''),
    alias: new FormControl('')
  })

  data = new Data();

  constructor(
    public dialogRef: MatDialogRef<ResourcesDialogComponent>,
    private formBuilder: FormBuilder) {
      this.form = this.formBuilder.group({
        production: ['', [Validators.required, blankSpaceValidator]],
        certification: ['', [Validators.required, blankSpaceValidator]],
        alias: ['']
      })
    }

  get getForm(){
      return this.form.controls;
  }

  ngOnInit(): void {
    console.log ('Abre dialogo');

  }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {

    this.data.production = this.form.controls.production.value;
    this.data.certification = this.form.controls.certification.value;
    this.data.alias = this.form.controls.alias.value;

    this.dialogRef.close(this.data);
  }
}

export class Data{
  production: String = '';
  certification: String = '';
  alias: String = ''
}
