import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

interface  ListSelected{
  codeValue: string;
  viewValue: string;
}

@Component({
  selector: 'app-steps-dialog',
  templateUrl: './steps-dialog.component.html',
  styleUrls: ['./steps-dialog.component.css']
})
export class StepsDialogComponent {

  form: FormGroup = new FormGroup({
    title: new FormControl(''),
    executor: new FormControl(''),
    subtitle: new FormControl(''),
    style: new FormControl(''),
    detail: new FormControl('')
  })

  executors: ListSelected[] = [
    {codeValue: '1', viewValue: 'AGILEOPS'},
    {codeValue: '2', viewValue: 'COS ACCESOS'},
    {codeValue: '3', viewValue: 'ANALISTA DE SEGURIDAD'},
    {codeValue: '4', viewValue: 'LÍDER TÉCNICO'},
  ];

  styles: ListSelected[] = [
    {codeValue: '1', viewValue: 'Texto'},
    {codeValue: '2', viewValue: 'Codigo'},
  ];

  data = new Data();

  constructor(
    public dialogRef: MatDialogRef<StepsDialogComponent>) {}

  ngOnInit(): void {


  }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.data.title = this.form.controls.title.value;
    this.data.executor = this.searchListExecutor(this.form.controls.executor.value);
    this.data.subtitle = this.form.controls.subtitle.value;
    this.data.style = this.searchListStyle(this.form.controls.style.value);
    this.data.detail = this.form.controls.detail.value;

    this.dialogRef.close(this.data);
  }

  searchListExecutor(index:String): any {
    let valor = this.executors.find(x => x.codeValue === index)
    return valor ;
  }

  searchListStyle(index:String): any {
    let valor = this.styles.find(x => x.codeValue === index)
    return valor ;
  }
}

export class Data{
  title: String = '';
  executor!: ListSelected;
  subtitle: String = '';
  style!: ListSelected;
  detail: String = '';
}
