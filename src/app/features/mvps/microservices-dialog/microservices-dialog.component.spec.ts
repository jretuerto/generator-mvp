import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroservicesDialogComponent } from './microservices-dialog.component';

describe('MicroservicesDialogComponent', () => {
  let component: MicroservicesDialogComponent;
  let fixture: ComponentFixture<MicroservicesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MicroservicesDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MicroservicesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
