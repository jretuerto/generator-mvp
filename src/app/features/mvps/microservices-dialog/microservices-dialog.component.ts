import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { blankSpaceValidator } from '../../../validators/custom.validators';

@Component({
  selector: 'app-microservices-dialog',
  templateUrl: './microservices-dialog.component.html',
  styleUrls: ['./microservices-dialog.component.css']
})

export class MicroservicesDialogComponent {

  form: FormGroup = new FormGroup({
    codeApplication: new FormControl(''),
    numberMvp: new FormControl(''),
    versionAtlas: new FormControl(''),
    suscriptionName: new FormControl(''),
    suscriptionId: new FormControl(''),
    microserviceName: new FormControl(''),
    releaseCandidate: new FormControl(''),
    resourceGroupProduction: new FormControl(''),
    resourceGroupCertification: new FormControl(''),
    nameAksProduction: new FormControl(''),
    nameAksCertification: new FormControl(''),
    repositoryConfigCertification: new FormControl(''),
    repositoryConfigProduction: new FormControl(''),
    jobNameCertification: new FormControl(''),
    jobNameProduction: new FormControl('')
  })

  data = new DataMicroservice();

  constructor(
    public dialogRef: MatDialogRef<MicroservicesDialogComponent>,
    private formBuilder: FormBuilder) {
      this.form = this.formBuilder.group({
        codeApplication: ['', [Validators.required, Validators.maxLength(4), blankSpaceValidator]],
        numberMvp: ['', [Validators.required, blankSpaceValidator]],
        versionAtlas: ['', Validators.required],
        suscriptionName: ['', [Validators.required]],
        suscriptionId: ['', [Validators.required]],
        microserviceName: ['', Validators.required],
        releaseCandidate: ['', [Validators.required]],
        resourceGroupProduction: ['', [Validators.required]],
        resourceGroupCertification: ['', Validators.required],
        nameAksProduction: ['', [Validators.required]],
        nameAksCertification: ['', [Validators.required]],
        repositoryConfigCertification: ['', Validators.required],
        repositoryConfigProduction: ['', [Validators.required]],
        jobNameCertification: ['', [Validators.required]],
        jobNameProduction: ['', Validators.required]
      })
    }

  get getForm(){
      return this.form.controls;
  }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.data.codeApplication = this.form.controls.codeApplication.value;
    this.data.numberMvp = this.form.controls.numberMvp.value;
    this.data.versionAtlas = this.form.controls.versionAtlas.value;
    this.data.suscriptionName = this.form.controls.suscriptionName.value;
    this.data.suscriptionId = this.form.controls.suscriptionId.value;
    this.data.microserviceName = this.form.controls.microserviceName.value;
    this.data.releaseCandidate = this.form.controls.releaseCandidate.value;

    this.data.resourceGroup = {
      certification: this.form.controls.resourceGroupCertification.value,
      production: this.form.controls.resourceGroupProduction.value
    };

    this.data.nameAks = {
      certification: this.form.controls.nameAksCertification.value,
      production: this.form.controls.nameAksProduction.value
    };

    this.data.repositoryConfig = {
      certification: this.form.controls.repositoryConfigCertification.value,
      production: this.form.controls.repositoryConfigProduction.value
    };

    this.data.jobName = {
      certification: this.form.controls.jobNameCertification.value,
      production: this.form.controls.jobNameProduction.value
    };

    this.dialogRef.close(this.data);
  }

  loadDataApplication(): void {

    if (this.form.controls.codeApplication.value === 'mbbk'){
      this.form.controls.suscriptionName.setValue('DTI - APP - BADI - Banca Digital');
      this.form.controls.suscriptionId.setValue('5ed45213-63d1-4c35-b3db-2cb1dc74e3f0');
      this.form.controls.resourceGroupCertification.setValue('RSGREU2BADIC01');
      this.form.controls.resourceGroupProduction.setValue('RSGREU2BADIP01');
      this.form.controls.nameAksCertification.setValue('aksveu2badic01');
      this.form.controls.nameAksProduction.setValue('aksveu2badip01');
    }

  }
}

export class DataMicroservice{
  codeApplication: string = '';
  numberMvp: string = '';
  versionAtlas: string = '';
  suscriptionName: string = '';
  suscriptionId: string = '';
  microserviceName: string = '';
  releaseCandidate: string = '';
  resourceGroup!: Environment;
  nameAks!: Environment | undefined;
  repositoryConfig!: Environment | undefined;
  jobName!: Environment | undefined;
}

interface Environment {
  certification: any;
  production: any;
}
