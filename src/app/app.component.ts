import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ResourcesDialogComponent } from './features/mvps/resources-dialog/resources-dialog.component';
import { StepsDialogComponent } from './features/mvps/steps-dialog/steps-dialog.component';
import { MatTable } from '@angular/material/table';
import { MicroservicesDialogComponent } from './features/mvps/microservices-dialog/microservices-dialog.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Maker MVP';
  patron = new RegExp('@{[A-Za-z0-9._%-]*}', 'g');

  @ViewChild(MatTable)
  tableResources!: MatTable<any>;

  displayedColumnsResources: string[] = ['productionColumn', 'certificationColumn', 'aliasColumn'];
  resources:Resource[] = [];
  steps:Step[] = [];
  microservices:Microservice[] = [];

  isMicroservice:boolean = false;

    constructor(public dialog: MatDialog) {}

    openDialogResource(){
      const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '370px';

      console.log('Open Dialog');
      const dialogRef = this.dialog.open(ResourcesDialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);

        if (result != null) {
          this.resources.push(result);
          console.log('The Array', this.resources);
          this.tableResources.renderRows();
        }

      });
    }

    openDialogStep(){
      const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '460px';

      console.log('Open Dialog');
      const dialogRef = this.dialog.open(StepsDialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);

        if (result != null) {
          this.steps.push(result);
          console.log('The Array Steps', this.steps);
        }

      });
    }

    openDialogMicroservices(){
      const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '640px';

      const dialogRef = this.dialog.open(MicroservicesDialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {

        if (result != null) {
          console.log("micro 1:", result);

          this.microservices.push(result);

          console.log("micro 2:", this.microservices);
        }

      });
    }

    replaceResourceProduction(paragraph:String):String {

      var sustuition:string = '';
      let founds = paragraph.match(this.patron);

      founds?.forEach(element => {
        sustuition = this.searchNameResourceProduction(element);
        paragraph = paragraph.replace(element, sustuition);
      })

      return paragraph;
    }

    replaceResourceCertification(paragraph:String):String {

      var sustuition:string = '';
      let founds = paragraph.match(this.patron);

      founds?.forEach(element => {
        sustuition = this.searchNameResourceCertification(element);
        paragraph = paragraph.replace(element, sustuition);
      })

      return paragraph;
    }

    searchNameResourceProduction(alias:string):string {
      let valor = this.resources.find(x => '@{' + x.alias + '}' === alias);

      return valor?.production!;
    }

    searchNameResourceCertification(alias:string):string {
      let valor = this.resources.find(x => '@{' + x.alias + '}' === alias);

      return valor?.certification!;
    }

}

export class Resource{
  production?: string = '';
  certification?: string = '';
  alias?: string = '';
}

export class Step{
  title: string = '';
  executor: ListSelected | undefined;
  subtitle: string = '';
  style: ListSelected | undefined;
  detail: string = '';
}
interface ListSelected {
  codeValue: string;
  viewValue: string;
}

export class Microservice{
  codeApplication: string = '';
  numberMvp: string = '';
  versionAtlas: string = '';
  suscriptionName: string = '';
  suscriptionId: string = '';
  microserviceName: string = '';
  releaseCandidate: string = '';
  resourceGroup!: Environment | undefined;
  nameAks!: Environment | undefined;
  repositoryConfig!: Environment | undefined;
  jobName!: Environment | undefined;
}
interface Environment {
  certification: any;
  production: any;
}
