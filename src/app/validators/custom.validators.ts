import { AbstractControl } from '@angular/forms';

export function blankSpaceValidator(control: AbstractControl) {
  if ((control.value as string).indexOf(' ') >= 0) {
    return { noBlankSpaceValid: true };
  }
  return null;
}
